﻿using System;
using CoreAnimation;
using KhodBalak;
using KhodBalak.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(DefaultEntry), typeof(DefaultEntryRenderer))]
namespace KhodBalak.iOS
{
	public class DefaultEntryRenderer : EntryRenderer
	{
		public DefaultEntryRenderer()
		{
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);

			if (Control != null)
			{
				Control.BorderStyle = UITextBorderStyle.None;
				Control.TextAlignment = UITextAlignment.Center;
				CALayer border = new CALayer();
				border.BorderWidth = 0.5f;
				border.BorderColor = UIColor.FromRGB(100,100,100).CGColor;
				border.Frame = new CoreGraphics.CGRect(0f, (Frame.Height / 2) + 4, Frame.Width, 0.5f);
				Control.Layer.AddSublayer(border);
				Control.Layer.MasksToBounds = true;
			}
		}
	}
}
