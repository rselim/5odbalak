﻿using System;

namespace KhodBalak.Services
{
	public interface INavigationService
	{
		void Configure(string pageKey, Type pageType);
		void Initialize(object navigation);
		void NavigateTo(string pageKey);
		void NavigateTo(string pageKey, object parameter);
	}
}
