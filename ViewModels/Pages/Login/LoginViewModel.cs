﻿using System;
using System.Threading.Tasks;
using KhodBalak.Services;
using TommasoScalici.MVVMExtensions.Commands;

namespace ViewModels
{
	public class LoginViewModel : ViewModelBase
	{
		IAppShell appShell;

		public LoginViewModel(IAppShell _appShell)
		{
			appShell = _appShell;
		}

		private IAsyncCommand _OnSignInCommand;
		public IAsyncCommand OnSignInCommand
		{
			get
			{
				if (_OnSignInCommand == null)
				{
					_OnSignInCommand = new AsyncCommand(OnSignIn);
				}
				return _OnSignInCommand;
			}
		}

		public async Task OnSignIn()
		{
			appShell.NavigateTo(Constants.Constants.Pages.ReportsLocationPage.ToString());
		}
	}
}
