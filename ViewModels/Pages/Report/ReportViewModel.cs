﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Constants;
using KhodBalak.Services;
using Models;
using TommasoScalici.MVVMExtensions.Commands;

namespace ViewModels
{
	public class ReportViewModel : ViewModelBase
	{
		IAppShell appShell;

		public ReportViewModel(IAppShell _appShell)
		{
			appShell = _appShell;

			var ReportTypesTemp = new List<ReportType>();
			ReportTypesTemp.Add(new ReportType() { Title = "Broken pipe", ImageUrl = "https://www.google.gr/images/branding/product/ico/googleg_lodp.ico" });
			ReportTypesTemp.Add(new ReportType() { Title = "dssds pipe", ImageUrl = "https://www.google.gr/images/branding/product/ico/googleg_lodp.ico" });
			ReportTypesTemp.Add(new ReportType() { Title = "test test", ImageUrl = "https://www.google.gr/images/branding/product/ico/googleg_lodp.ico" });

			ReportTypes = ReportTypesTemp;
		}

		private List<ReportType> _ReportTypes;
		public List<ReportType> ReportTypes
		{
			get { return _ReportTypes; }
			set { _ReportTypes = value;}
		}

		private IAsyncCommand _OnItemTappedCommand;
		public IAsyncCommand OnItemTappedCommand
		{
			get
			{
				if (_OnItemTappedCommand == null)
				{
					_OnItemTappedCommand = new AsyncCommand(OnItemTapped);
				}
				return _OnItemTappedCommand;
			}
		}

		public async Task OnItemTapped()
		{
			appShell.NavigateTo(Constants.Constants.Pages.ReportsLevelPage.ToString());
		}

		private IAsyncCommand _OnUseCurrentLocationCommand;
		public IAsyncCommand OnUseCurrentLocationCommand
		{
			get
			{
				if (_OnUseCurrentLocationCommand == null)
				{
					_OnUseCurrentLocationCommand = new AsyncCommand(OnUseCurrentLocation);
				}
				return _OnUseCurrentLocationCommand;
			}
		}

		public async Task OnUseCurrentLocation()
		{
			appShell.NavigateTo(Constants.Constants.Pages.ReportsPage.ToString());
		}

		private AsyncCommand _OnInsertLocationCommand;
		public AsyncCommand OnInsertLocationCommand
		{
			get
			{
				if (_OnInsertLocationCommand == null)
				{
					_OnInsertLocationCommand = new AsyncCommand(OnInsertLocation);
				}
				return _OnInsertLocationCommand;
			}
		}

		public async Task OnInsertLocation()
		{
			appShell.NavigateTo(Constants.Constants.Pages.ReportsPage.ToString());
		}

		//private IAsyncCommand<object> _OnReportLevelCommand;
		//public IAsyncCommand<object> OnReportLevelCommand
		//{
		//	get
		//	{
		//		if (_OnReportLevelCommand == null)
		//		{
		//			_OnReportLevelCommand = new AsyncCommand<object>(OnReportLevel);
		//		}
		//		return _OnReportLevelCommand;
		//	}
		//}

		//public async Task OnReportLevel(object level)
		//{

		//}
	}
}
