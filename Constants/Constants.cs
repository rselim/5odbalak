﻿using System;
namespace Constants
{
	public class Constants
	{
		public enum Pages
		{
			SigninPage,
			ReportsPage,
			ReportsLevelPage,
			ReportsLocationPage
		}
	}
}
