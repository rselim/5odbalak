﻿using System;
using ViewModels;
using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;
using KhodBalak.Services;

namespace KhodBalak
{
	public class ViewModelLocator
	{
		public ViewModelLocator()
		{
			ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

			SimpleIoc.Default.Register<IAppShell, AppShell>();

			SimpleIoc.Default.Register<LoginViewModel>();
			SimpleIoc.Default.Register<ReportViewModel>();
		}

		public LoginViewModel Login
		{
			get
			{
				return ServiceLocator.Current.GetInstance<LoginViewModel>();
			}
		}

		public ReportViewModel Report
		{
			get
			{
				return ServiceLocator.Current.GetInstance<ReportViewModel>();
			}
		}
	}
}
