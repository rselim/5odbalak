﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using KhodBalak.Services;
using Microsoft.Practices.ServiceLocation;
using Xamarin.Forms;

namespace KhodBalak
{
	public partial class App : Application
	{
		public static AppShell Shell { get; private set; }

		private static ViewModelLocator _locator;

		public static ViewModelLocator Locator
		{
			get
			{
				return _locator ?? (_locator = new ViewModelLocator());
			}
			set
			{
				_locator = value;
			}
		}

		public App()
		{
			InitializeComponent();

			Locator = new ViewModelLocator();

			Shell = ServiceLocator.Current.GetInstance<IAppShell>() as AppShell;
			Shell.Configure(Constants.Constants.Pages.SigninPage.ToString(), typeof(SigninPage));
			Shell.Configure(Constants.Constants.Pages.ReportsPage.ToString(), typeof(ReportPage));
			Shell.Configure(Constants.Constants.Pages.ReportsLevelPage.ToString(), typeof(ReportLevelPage));
			Shell.Configure(Constants.Constants.Pages.ReportsLocationPage.ToString(), typeof(ReportLocationPage));


			MainPage = new NavigationPage(Shell) { BarBackgroundColor = Color.FromHex("#f2b827"),BarTextColor= Color.FromHex("#fff")};
			Shell.Initialize(MainPage);
			Shell.NavigateTo(Constants.Constants.Pages.SigninPage.ToString());
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
