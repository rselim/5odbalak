﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace KhodBalak
{
	public partial class SigninPage : ContentPage
	{
		public SigninPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
			BindingContext = App.Locator.Login;
		}
	}
}
