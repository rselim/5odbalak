﻿using System;
using System.Collections.Generic;
using DLToolkit.Forms.Controls;
using Xamarin.Forms;

namespace KhodBalak
{
	public partial class ReportPage : ContentPage
	{
		public ReportPage()
		{
			InitializeComponent();

			FlowListView.Init();
			BindingContext = App.Locator.Report;
		}
	}
}
