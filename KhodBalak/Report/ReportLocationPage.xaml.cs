﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace KhodBalak
{
	public partial class ReportLocationPage : ContentPage
	{
		public ReportLocationPage()
		{
			InitializeComponent();

			BindingContext = App.Locator.Report;
		}
	}
}
