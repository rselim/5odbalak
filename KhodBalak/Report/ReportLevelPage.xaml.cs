﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace KhodBalak
{
	public partial class ReportLevelPage : ContentPage
	{
		public ReportLevelPage()
		{
			InitializeComponent();

			BindingContext = App.Locator.Report;
		}
	}
}
