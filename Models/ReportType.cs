﻿using System;
namespace Models
{
	public class ReportType
	{
		public string Title { get; set; }
		public string ImageUrl { get; set; }
	}
}
